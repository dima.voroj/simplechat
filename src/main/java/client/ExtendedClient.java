package client;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;

import static java.lang.String.format;
import static java.net.URLEncoder.encode;
import static java.nio.charset.Charset.defaultCharset;

public class ExtendedClient {
    private static long timestamp;

    private static Scanner scanner = new Scanner(System.in);
    private static String messageFormat = "(%s) %s -> %s";
    private static String loginFormat = "http://localhost:4567/chat/login/%s/%s";
    private static String urlFormat = "http://localhost:4567/chat/message?message=%s&sessionId=%s";

    private static CloseableHttpClient httpClient = HttpClients.createDefault();
    private static BasicResponseHandler basicResponseHandler = new BasicResponseHandler();

    public static void main(String[] args) {
        System.out.println("1 - Продолжить как аноним");
        System.out.println("2 - Залогиниться");
        String sessionId = "";
        if (scanner.nextInt() == 2) {
            sessionId = request(format(loginFormat, scanner.next(), scanner.next()));
        }
        printMessages();
        watchNewMessages();

        String message;
        while (true) {
            message = scanner.nextLine();
            if (!message.equals("!q")) {
                request(format(urlFormat, encode(message, defaultCharset()), sessionId));
            } else {
                break;
            }
        }
    }

    private static void watchNewMessages() {
        new Thread(() -> {
            while (true) {
                printMessages();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Thread.currentThread().interrupt();
                }
            }
        }).start();
    }

    private static String request(String url) {
        HttpGet httpGet = new HttpGet(url);
        try (CloseableHttpResponse httpResponse = httpClient.execute(httpGet)) {
            return basicResponseHandler.handleResponse(httpResponse);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private static void printMessages() {
        String[] messages = request("http://localhost:4567/chat/messages?time=" + timestamp).split("<br>");
        Arrays.stream(messages).filter(s -> !s.isEmpty()).map(m -> {
            String[] message = m.split(",");
            return format(messageFormat, new Date(Long.parseLong(message[2])), message[0], message[1]);
        }).forEach(System.out::println);

        timestamp = System.currentTimeMillis();
    }
}
