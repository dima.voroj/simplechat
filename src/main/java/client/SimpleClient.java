package client;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.util.Scanner;

public class SimpleClient {
    private static CloseableHttpClient httpClient = HttpClients.createDefault();
    private static BasicResponseHandler basicResponseHandler = new BasicResponseHandler();
    private static String name;
    private static String addMessageUrl = "http://localhost:4567/chat/message/%s?message=%s";
    private static long lastRequest;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Здравствуйте, пожалуйста введите свое имя");
        name = scanner.next();
        int num;
        do {
            System.out.println("1 - показать все сообщения");
            System.out.println("2 - ввести сообщение");
            System.out.println("3 - обновить сообщения");
            System.out.println("0 - выйти");
            num = scanner.nextInt();
            switch (num) {
                case 1:
                    printMessages(0);
                    break;
                case 2:
                    System.out.println(request(String.format(addMessageUrl, name, scanner.next())));
                    break;
                case 3:
                    printMessages(lastRequest);
                    break;
                default:
                    System.out.println("Неизвестная команда");
            }
        } while (num != 0);
    }

    private static String request(String url) {
        HttpGet httpGet = new HttpGet(url);
        try (CloseableHttpResponse httpResponse = httpClient.execute(httpGet)) {
            return basicResponseHandler.handleResponse(httpResponse);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private static void printMessages(long timestamp) {
        System.out.println(request("http://localhost:4567/chat/messages?time=" + timestamp).replace("<br>", "\n"));
        lastRequest = System.currentTimeMillis();
    }
}
