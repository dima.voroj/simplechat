package server;

import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static spark.Spark.get;

public class Server {
    public static void main(String[] args) {
        List<Message> messages = new ArrayList<>();
        Set<User> users = new HashSet<>();
        Map<String, User> sessions = new HashMap<>();
        Random rand = new Random();

        messages.add(new Message("Chat start", "admin", 10_000_000));

        get("/chat/messages", (request, response) -> {
            String time = request.queryParams("time");
            if (time != null) {
                try {
                    long timestamp = Long.parseLong(time);
                    return toString(messages.stream().filter(m -> m.getTimestamp() > timestamp));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    response.status(400);
                    return "";
                }
            } else {
                return toString(messages.stream());
            }
        });

        get("/chat/message", (request, response) -> {
            User user = sessions.get(request.queryParams("sessionId"));
            String text = request.queryParams("message");
            if (!text.isEmpty()) {
                Message message = new Message(text, user == null ? "anon" : user.getName(), System.currentTimeMillis());
                messages.add(message);
                System.out.println(message);
            }
            response.status(200);
            return "";
        });

        get("/chat/register/:username/:password", (request, response) -> {
            User user = new User(request.params("username"), request.params("password"));
            response.status(users.add(user) ? 200 : 409);
            System.out.println(user);
            return "";
        });

        get("/chat/login/:username/:password", (request, response) -> {
            User user = new User(request.params("username"), request.params("password"));
            if(users.stream().anyMatch(u -> u.equals(user))) {
                String sessionId = new BigInteger(128, rand).toString(16);

                sessions.put(sessionId, user);
                response.status(200);
                return sessionId;
            } else {
                response.status(401);
                return "";
            }
        });
    }

    private static String toString(Stream<Message> messages) {
        return messages.map(Message::toString).collect(Collectors.joining("<br>"));
    }
}
