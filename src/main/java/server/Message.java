package server;

public class Message {
    private final String text;
    private final String user;
    private final long timestamp;

    public Message(String text, String user, long timestamp) {
        this.text = text;
        this.user = user;
        this.timestamp = timestamp;
    }

    public String getText() {
        return text;
    }

    public String getUser() {
        return user;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return user + "," + text + "," + timestamp;
    }
}
